calculator2.0
-------------

A calculator that can operate multiple values at once.

.. code:: python

  Allowed Operations:
  => Addition(+)
  => Subtraction(-)
  => Multiplication(*)
  => Division(/)

  : 2+8*5-6
  

  Answer: 36.0

